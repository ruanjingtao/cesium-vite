import emitter from "mitt";

const bus = emitter()
export const EVENT_TYPE = {
    CLICK: '点击'
}
export const $on = bus.on
export const $off = bus.off
export const $emit = bus.emit
