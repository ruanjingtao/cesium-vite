let now = Date.now()
export const lines = [
    {
        timestamp: now,
        longitude: 111.20359    ,
        latitude: 30.6845475,
        height: 2
    },
    {
        timestamp: now+1000,
        longitude: 111.20428902828806,
        latitude: 30.684060211933243,
        height: 2
    },
    {
        timestamp: now+2000,
        longitude: 111.2041564557763,
        latitude: 30.683829869991417,
        height: 2
    },
    {
        timestamp: now+2000,
        longitude: 111.2042470569484,
        latitude: 30.683735312234003,
        height: 2
    },
    {
        timestamp: now+3000,
        longitude: 111.20440530,
        latitude: 30.6836863,
        height: 3
    },
    {
        timestamp: now+4000,
        longitude: 111.2049981017,
        latitude: 30.6835761,
        height: 5
    },
    {
        timestamp: now+5000,
        longitude: 111.2055137,
        latitude: 30.6836684,
        height: 5
    },
    {
        timestamp: now+5000,
        longitude: 111.20587189920106,
        latitude: 30.6841628,
        height: 5
    },
    {
        timestamp: now+5000,
        longitude: 111.20548340722868,
        latitude: 30.68490368315733,
        height: 5
    },
    {
        timestamp: now+5000,
        longitude: 111.20485499430511,
        latitude: 30.68523871423592,
        height: 12
    },
    {
        timestamp: now+5000,
        longitude: 111.20397135919902,
        latitude: 30.685332355,
        height: 10
    },
    {
        timestamp: now+5000,
        longitude: 111.20374120132126,
        latitude: 30.684933834735038,
        height: 10
    },
]