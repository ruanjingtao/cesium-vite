import { defineStore } from 'pinia'

export const useStore = defineStore('storeId',{
    state:() => {
        return {
            name: 'rjt',
            age: 25
        }
    },
    actions:{
        incrementAge(){
            this.age++
        }
    },
    getters:{
        changeAge(){
            console.log('getters')
            return this.age + 100
        }
    }
})