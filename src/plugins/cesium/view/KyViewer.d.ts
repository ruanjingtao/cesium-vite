import * as Cesium from "cesium";

interface Position{
    longitude: number
    latitude: number
    height: number
    heading?: number
    pitch?: number
    roll?: number
}
export class KyViewer extends Cesium.Viewer{
    /**
     * 构造方法
     * @param elementId 加载三维地图的div的id (required)
     * @param options 需要重新设置的cesium.Viewer的原生属性，可以不传
     */
    constructor(elementId: string, options?: Object): void

    /**
     * 显示地球、天空、太阳，月亮等
     */
    showGlobe() :void

    /**
     * 关闭显示地球、天空、太阳，月亮等
     */
    hideGlobe() :void

    /**
     * 设置导航条,参数为相机2
     */
    setNavigation(camera: Object) :void

    setCameraView(position: object) :void

    /**
     * 设置摄像机飞到指定球
     * @param sphere
     * @param options
     */
    setCameraFlyToBoundingSphere(sphere, options) :void

    /**
     * 设置相机飞行至指定位置
     * @param position.longitude 经度
     * @param position.latitude 纬度
     * @param position.height 高度（米）
     * @param position.heading 旋转
     * @param position.pitch 旋转
     * @param position.roll 旋转
     * @param position.duration 飞行时间
     * @param position
     * @param completeFun 完成飞行之后的回调函数
     */
    setCameraFlyTo(position,completeFun?:Function) :void

    /**
     * 销毁kyviewer实例
     */
    destroy() :void

    /**
     * 左键点击事件（返回坐标）
     * @param callback 事件回调
     */
    onClick(callback: Function) :void

    /**
     * 鼠标移动事件 (返回坐标)
     * @param callback
     */
    onMove(callback) :void

    /**
     * 右键点击事件
     * @param callback 事件回调
     */
    onRightClick(callback: Function) :void

    /**
     * 左键双击事件
     * @param callback 事件回调
     */
    onLeftDoubleClick(callback) :void

    /**
     * 鼠标中键滚轮的点击事件
     * @param callback
     */
    onMiddleClick(callback) :void

    /**
     * 点击Feature的事件（返回Feature的id)
     * @param callback  回调方法 (required)
     */
    onClickFeature(callback) :void

    /**
     * 销毁鼠标事件
     */
    destroyHandler() :void
}