/**
 * 不同环境访问不同的3dtiles地址
 * @return {string}
 */
export const getTilesUrl = () => {
  let tilesUrl = '/3ds/tileset.json'

  return tilesUrl
}

// cesium上工厂模型
// const url = 'http://10.11.2.46:8200/tiles/factory/tileset.json'
// const url = 'http://10.12.1.41:8200/tiles/factory/tileset.json'

// 山东某化工公司
// const url = 'http://192.168.1.12:8001/3d/shandongqifa/tileset.json'
//
// const url = 'http://192.168.1.12:8001/3d/Tiles/tileset.json'

// 宜昌东土部分模型
// let url = 'http://192.168.1.12:8001/3d/terra_b3dms/BlockY/tileset.json'

// 测试管道建模
// let url = 'http://192.168.1.12:8001/3d/3dtiles-test/tileset.json'

// 白沙河
// let url = 'http://10.11.2.46:8200/tiles/baishahe20/tileset.json'
// const url = 'http://10.11.2.46:8200/tiles/baishaheL/tileset.json'
// let url = 'http://192.168.1.12:8001/3d/terra_b3dms/baihsaheKH/tileset.json'
