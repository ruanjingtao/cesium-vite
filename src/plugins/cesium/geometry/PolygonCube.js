// import * as Cesium from 'cesium'
import { PolygonStyle } from '../index'
/**
 * 带高度的多边形
 */
export default class PolygonCube {
  /**
   * 构造方法
   * @param options
   * viewer required
   * coordinates 坐标数组[x,y,z,x,y,z ...] required
   * id  唯一id
   * height 拉伸高度（棱柱的高度） 默认 10
   * extrudedHeight 底面距离地球球面的高度 默认 10
   * style 样式设置 详见 PolygonStyle
   */
  constructor(options) {
    let { viewer, id, coordinates, height, extrudedHeight, style } = options || {}
    this.viewer = viewer
    this.id = id
    this.height = height || 20
    this.coordinates = coordinates
    this.extrudedHeight = extrudedHeight || 0
    height = this.height + this.extrudedHeight
    style = style || new PolygonStyle()
    this.polygon = viewer.entities.add({
      id,
      polygon: {
        height: height + this.extrudedHeight,
        hierarchy: Cesium.Cartesian3.fromDegreesArrayHeights(coordinates),
        extrudedHeight: this.extrudedHeight,
        // 是否使用坐标定位中的高度值
        // perPositionHeight: false,
        material: style.color,
        // outline: style.outline,
        outlineColor: style.outlineColor
      }
      // classification: Cesium.ClassificationPrimitive
    })
  }

  /**
   * 设置坐标，重新渲染
   */
  setCoordinates(coordinates) {
    this.polygon.polygon.hierarchy = Cesium.Cartesian3.fromDegreesArrayHeights(coordinates)
  }

  /**
   * 获取当前多边形区域的坐标数组
   */
  getCoordinates() {
    return this.coordinates
  }

  /**
   * 获取多边形高度
   * @returns {number} 多边形高度
   */
  getHeight() {
    return this.height
  }

  /**
   * 获取多边形基底高度
   * @returns {number}
   */
  getExtrudedHeight() {
    return this.extrudedHeight
  }

  /**
   * 设置拉伸高度
   * @param height required
   */
  setHeight(height) {
    this.height = height
    const resHeight = height + this.extrudedHeight
    this.polygon.polygon.height = resHeight
  }

  /**
   * 底面距离地球球面的高度
   * @param extrudedHeight required
   */
  setExtrudedHeight(extrudedHeight) {
    this.extrudedHeight = extrudedHeight
    this.polygon.polygon.extrudedHeight = extrudedHeight
    const resHeight = this.height + extrudedHeight
    this.polygon.polygon.height = resHeight
  }

  /**
   * 显示隐藏该实体
   * @param visibility bool量是否显示
   */
  setVisible(visibility) {
    this.polygon.polygon.show = visibility
  }

  /**
   * 设置样式
   * @param style  详见 PolygonStyle
   */
  setStyle(style) {
    style = style || new PolygonStyle()
    this.polygon.polygon.material = style.color
    this.polygon.polygon.outline = style.outline
    this.polygon.polygon.outlineColor = style.outlineColor
  }

  /**
   * 从场景中删除该几何体
   */
  remove() {
    this.viewer.entities.remove(this.polygon)
  }
}
