// import * as Cesium from 'cesium'
import mitt from 'mitt'

export default class Entity {
  // 实体类型
  static ENTITY_TYPE = {
    POINT: 'point',
    BILLBOARD: 'billboard'
  }

  // 订阅消息类型
  static EVENT_TYPE = {
    MOVE_END: 'moveEnd'
  }

  constructor(viewer) {
    this.viewer = viewer
    this.marker = null // 标记点实例
    this.markerType = '' // 标记点类型
    this.id = null // 标记点id
    this.startPosition = null // 起点
    this.isMoving = false // 改变位置时，是否正在执行
    this.emitter = mitt()
  }

  /**
   * 创建点
   */
  createPoint(options) {
    this.markerType = Entity.ENTITY_TYPE.POINT
    this.startPosition = options.position
    this.marker = this.viewer.entities.add({
      ...options,
      point: {
        pixelSize: 4, //点的大小
        color: Cesium.Color.RED //点的颜色
      }
    })
    this.id = this.marker.id
  }

  /**
   * 创建图片标记
   */
  createBillboard({ position, id, iconStyle, textStyle }) {
    this.markerType = Entity.ENTITY_TYPE.BILLBOARD
    this.startPosition = position
    this.marker = this.viewer.entities.add({
      position,
      id,
      billboard: {
        image: iconStyle.url,
        width: iconStyle.width,
        height: iconStyle.height,
        pixelOffset: iconStyle.pixelOffset,
        verticalOrigin: Cesium.VerticalOrigin.BOTTOM
        // heightReference: Cesium.HeightReference.CLAMP_TO_GROUND
      },
      label: {
        text: textStyle.text,
        font: textStyle.font,
        fillColor: textStyle.fillColor,
        outlineColor: textStyle.outlineColor,
        outlineWidth: textStyle.outlineWidth,
        showBackground: textStyle.showBackground,
        backgroundColor: textStyle.backgroundColor,
        style: Cesium.LabelStyle.FILL_AND_OUTLINE,
        // 垂直方向以底部来计算标签的位置
        verticalOrigin: Cesium.VerticalOrigin.BOTTOM,
        // heightReference: Cesium.HeightReference.CLAMP_TO_GROUND,
        // 偏移量
        pixelOffset: textStyle.pixelOffset
      }
    })
    this.id = this.marker.id
  }

  /**
   * 移动到某个点
   * @param position 点位置
   * @param animation 是否开启动画平移
   */
  moveTo(position, animation = false) {
    // 如果点不存在或者移动后的点和当前点一样，则不操作
    if (!this.marker || JSON.stringify(this.startPosition) === JSON.stringify(position)) {
      return
    }
    if (!animation) {
      this.marker.position = position
      return
    }
    let factor = 0
    this.isMoving = true
    const t = Date.now()
    let isEmit = false
    this.marker.position = new Cesium.CallbackProperty(() => {
      if (factor >= 50) {
        if (!isEmit) {
          // console.log('触发动画结束事件，动画执行时长：' + (Date.now() - t) + 'ms')
          this.isMoving = false
          this.startPosition = position
          this.emitter.emit(Entity.EVENT_TYPE.MOVE_END)
          isEmit = true
        }
        return position
      }
      factor++
      // 动态更新位置
      return Cesium.Cartesian3.lerp(this.startPosition, position, factor / 50.0, new Cesium.Cartesian3())
    }, false)
  }

  /**
   * 从场景中删除该标记
   */
  remove() {
    this.viewer.entities.remove(this.marker)
  }

  /**
   * 订阅位置移动状态
   */
  addListener(eventName, callback) {
    if (!Object.values(Entity.EVENT_TYPE).includes(eventName)) {
      return
    }
    this.emitter.on(eventName, callback)
  }

  /**
   * 取消订阅
   * @param eventName 事件名，为空时全部取消
   */
  removeListener(eventName) {
    if (eventName) {
      this.emitter.off(eventName)
      return
    }
    Object.values(Entity.EVENT_TYPE).forEach((item) => {
      this.emitter.off(item)
    })
  }
}
