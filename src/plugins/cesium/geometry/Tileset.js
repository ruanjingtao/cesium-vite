// import { Cesium3DTileset } from 'cesium'
// import * as Cesium from 'cesium'

/**
 * 3dTiles加载以及相关操作
 */
export default class Tileset extends Cesium.Cesium3DTileset {
  /**
   * 构造方法
   * @param url 3dTiles的资源路径 required
   * @param viewer required
   * @param options 扩展参数，详见 https://cesium.com/learn/cesiumjs/ref-doc/Cesium3DTileset.html?classFilter=tileset
   * @returns {module:cesium.Cesium3DTileset}
   */
  constructor(url, viewer, options) {
    const initOpt = {
      url,
      shadows: Cesium.ShadowMode.DISABLED,
      preferLeaves: false,
      maximumScreenSpaceError: 64,
      ...options
    }
    console.log('此时tileset初始化参数', initOpt)
    // debugger
    super(initOpt)
    this.tilesetPrimitive = viewer.scene.primitives.add(this)
    this.viewer = viewer
    this.loading = true
    // 事件注册
    this.handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas)
  }

  /**
   * 加载完成的回调方法
   * @param callback
   */
  onLoad(callback) {
    this.initialTilesLoaded.addEventListener(function (tile) {
      // const content = tile.content
      // const featureLength = content.featuresLength
      // console.log(featureLength)
      callback(tile)
    })
  }

  /**
   * 模型的左键点击事件
   */
  onClickTileset(callback) {
    this.handler.setInputAction(callback, Cesium.ScreenSpaceEventType.LEFT_CLICK)
  }

  /**
   * 模型的鼠标移动事件
   * @param callback
   */
  onMoveTileset(callback) {
    this.handler.setInputAction(callback, Cesium.ScreenSpaceEventType.MOUSE_MOVE)
  }

  /**
   * 模型的鼠标右键点击事件
   * @param callback
   */
  onRightClickTileset(callback) {
    this.handler.setInputAction(callback, Cesium.ScreenSpaceEventType.RIGHT_CLICK)
  }

  /**
   * 模型的鼠标左键双击事件
   * @param callback
   */
  onLeftDoubleClickTileset(callback) {
    this.handler.setInputAction(callback, Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK)
  }

  /**
   * 模型的元素点击事件
   */
  onClickTileFeature(callback) {
    this.handler.setInputAction((movement) => {
      const feature = this.viewer.scene.pick(movement.position)
      if (feature instanceof Cesium.Cesium3DTileFeature) {
        const featureId = feature.id
        const featureName = feature.getPropertyNames()
        callback(feature)
      }
    }, Cesium.ScreenSpaceEventType.LEFT_CLICK)
  }

  /**
   * 已知需要移动的偏移量,对3dtile进行平移
   * @param cartesian3 已知要移动的偏移量 x,y,z
   */
  MoveByCartesian(x, y, z) {
    // 已知笛卡尔偏移量 创建平移矩阵方法
    const translation = Cesium.Cartesian3.fromArray([x, y, z])
    const m = Cesium.Matrix4.fromTranslation(translation)
    this.modelMatrix = m
  }

  /**
   * 旋转3dtiles指定角度
   * @param rx
   * @param ry
   * @param rz
   */
  rotateByDegree({rx = 0, ry = 0, rz = 0}) {
    //旋转
    const mx = Cesium.Matrix3.fromRotationX(Cesium.Math.toRadians(rx))
    const my = Cesium.Matrix3.fromRotationY(Cesium.Math.toRadians(ry))
    const mz = Cesium.Matrix3.fromRotationZ(Cesium.Math.toRadians(rz))
    const rotationX = Cesium.Matrix4.fromRotationTranslation(mx)
    const rotationY = Cesium.Matrix4.fromRotationTranslation(my)
    const rotationZ = Cesium.Matrix4.fromRotationTranslation(mz)
    //平移 还在原地
    const position = this.boundingSphere.center
    const m = Cesium.Transforms.eastNorthUpToFixedFrame(position)
    //旋转、平移矩阵相乘
    Cesium.Matrix4.multiply(m, rotationX, m)
    Cesium.Matrix4.multiply(m, rotationY, m)
    Cesium.Matrix4.multiply(m, rotationZ, m)
    //赋值给tileset
    this.root.transform = m
  }

  /**
   * 已知经纬度,把3dtile模型平移至目标经纬度处
   * @constructor
   */
  MoveByCartographic(long, lati, height) {
    const tilePostion = Cesium.Cartesian3.fromDegrees(long, lati, height)
    const m = Cesium.Transforms.eastNorthUpToFixedFrame(tilePostion)

    this.root.transform = m
  }

  /**
   * 销毁模型的鼠标事件
   */
  destroyTilesetHandler() {
    this.handler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_CLICK)
    this.handler.removeInputAction(Cesium.ScreenSpaceEventType.MOUSE_MOVE)
    this.handler.removeInputAction(Cesium.ScreenSpaceEventType.RIGHT_CLICK)
  }

  /**
   * 自动缩放，让模型在当前屏幕完全显示
   */
  zoomToScreen() {
    this.viewer.zoomTo(this.tilesetPrimitive)
    this.viewer.camera.setView({
      destination: {
        x: -2029197.6342666247,
        y: 5120831.567717429,
        z: 3205112.791262487
      },
      orientation: {
        heading: 138.3569508230061,
        pitch: -41.143907108092186,
        roll: 0.0002761094447412272
      }
    })
    return this.viewer.scene.camera
  }

  /**
   * 设置相机位置
   */
  setCameraView(position) {
    console.log('开始设置相机位置,参数', position)
    this.viewer.camera.setView({
      destination: { x: position.x, y: position.y, z: position.z },
      orientation: { heading: position.heading, pitch: position.pitch, roll: position.roll }
    })
  }
}
