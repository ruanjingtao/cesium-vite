// import * as Cesium from 'cesium'

/**
 * 绘制围墙的方法
 * @param posArray 位置数组[经度，纬度，经度，纬度...]
 * @param minHeight 围墙最低高度
 * @param maxHeight 围墙最高高度
 * @param rgba css格式颜色字符串，例： '#03E070'
 */
export default class Wall {
  /**
   *
   * @param options.posArray 位置数组[经度，纬度，经度，纬度...]
   * @param options.minHeight 围墙最低高度
   * @param options.maxHeight 围墙最高高度
   * @param options.rgba css格式颜色字符串，例： '#03E070'
   */
  constructor(options) {
    const {viewer,id,posArray,minHeight,maxHeight,rgba}=options || {}
    this.rgba = Cesium.Color.fromCssColorString('#fa7878')
    if (rgba) {
      this.rgba = Cesium.color.fromCssColorString(rgba)
    }
    this.a = null
    this.markerOpacity = 1
    this.id = id
    this.viewer = viewer
    this.posArray = posArray
    this.minHeightArr = []
    this.maxHeightArr = []
    for (let i = 0; i < posArray.length/2; i++) {
      this.minHeightArr.push(minHeight)
      this.maxHeightArr.push(maxHeight)
    }
    this.wall = viewer.entities.add({
      id: `wall-${id}`,
      wall: {
        positions: Cesium.Cartesian3.fromDegreesArray(posArray),
        maximumHeights: this.maxHeightArr,
        minimumHeights: this.minHeightArr,
        material: new Cesium.ImageMaterialProperty({
          transparent: true, //设置透明
          image: this.getColorRamp({
            0.0: this.rgba.withAlpha(1.0).toCssColorString().replace(')', ',1.0)'),
            0.045: this.rgba.withAlpha(0.8).toCssColorString(),
            0.1: this.rgba.withAlpha(0.6).toCssColorString(),
            0.15: this.rgba.withAlpha(0.4).toCssColorString(),
            0.37: this.rgba.withAlpha(0.2).toCssColorString(),
            0.54: this.rgba.withAlpha(0.1).toCssColorString(),
            1.0: this.rgba.withAlpha(0).toCssColorString()
          }),
          // color: new Cesium.CallbackProperty(() => {
          //   // console.log('我在回调改颜色')
          //   return (
          //     this.a ? ((this.markerOpacity -= 0.01), this.markerOpacity <= 0.3 && (this.a = false)) : ((this.markerOpacity = 1), (this.a = true)),
          //       Cesium.Color.WHITE.withAlpha(this.markerOpacity)
          //   )
          // }, false)
        })
      }
    })
    console.log('wall的构造函数', this.wall)
  }

  getColorRamp(val) {
    if (val === null) {
      val = { 0.0: 'blue', 0.1: 'cyan', 0.37: 'lime', 0.54: 'yellow', 1: 'red' }
    }
    let ramp = document.createElement('canvas')
    ramp.width = 1
    ramp.height = 100
    let ctx = ramp.getContext('2d')
    let grd = ctx.createLinearGradient(0, 0, 0, 100)
    for (let key in val) {
      grd.addColorStop(1 - Number(key), val[key])
    }
    ctx.fillStyle = grd
    ctx.fillRect(0, 0, 1, 100)
    return ramp
  }

  /**
   * 设置wall是否可见
   * @param bool visibility
   */
  setVisible(visibility) {
    this.wall.show = visibility
  }

  /**
   * 获取当前多边形区域的坐标数组
   */
  getCoordinates() {
    return this.posArray
  }

  /**
   * 从场景中删除该围墙
   */
  remove() {
    this.viewer.entities.remove(this.wall)
  }
}
