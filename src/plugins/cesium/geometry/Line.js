// import * as Cesium from 'cesium'
import LineStyle from '../style/LineStyle'

/**
 * 线（贴地）
 */
export default class Line {
  /**
   * 构造方法
   * @param options
   * coordinates 坐标 [lon,lat,lon,lat...]
   * id 唯一标识
   * viewer
   * style 线的样式 详见 LineStyle
   */
  constructor(options) {
    let { viewer, id, coordinates, style } = options || {}
    style = style || new LineStyle()
    this.viewer = viewer
    const positions = Cesium.Cartesian3.fromDegreesArray(coordinates)
    const material = this._getMaterial(style)
    this.line = viewer.entities.add({
      id,
      polyline: {
        positions: positions,
        width: style.width,
        arcType: Cesium.ArcType.RHUMB,
        clampToGround: true,
        material
      }
    })
  }

  /**
   * 构造  material
   * @param style
   * @return {*|module:cesium.PolylineDashMaterialProperty}
   * @private
   */
  _getMaterial(style) {
    if (style.dash) {
      return new Cesium.PolylineDashMaterialProperty({
        dashLength: style.dashLength,
        color: style.dashColor,
        gapColor: style.dashGapColor
      })
    } else {
      return style.color
    }
  }

  /**
   * 设置坐标
   * @param coordinates 坐标  [lon,lat,lon,lat...]
   */
  setCoordinates(coordinates) {
    this.line.polyline.positions = Cesium.Cartesian3.fromDegreesArray(coordinates)
  }

  /**
   * 添加坐标
   * @param lon 经度
   * @param lat 纬度
   */
  addCoordinate(lon, lat) {
    // debugger
    this.line.polyline.positions.getValue().push(Cesium.Cartesian3.fromDegrees(lon, lat))
  }

  /**
   * 设置样式
   * @param style 线的样式 详见 LineStyle
   */
  setStyle(style) {
    style = style || new LineStyle()
    this.line.polyline.width = style.width
    this.line.polyline.material = this._getMaterial(style)
  }

  /**
   * 从场景中移除
   */
  remove() {
    this.viewer.entities.remove(this.line)
  }
}
