// import * as Cesium from 'cesium'
import carImg from '@/assets/mapIcon/car.png'
import personImg from '@/assets/locateImage/trackPerson.png'

Cesium.Timeline.prototype.makeLabel = function (time) {
  return formatDate(Cesium.JulianDate.toDate(time), 'HH:mm:ss')
}
function formatDate(date, format = 'YYYY-MM-DD HH:mm:ss') {
  var year = date.getFullYear()
  var month = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1
  var day = date.getDate() < 10 ? '0' + date.getDate() : date.getDate()
  var hours = date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
  var minutes = date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()
  var seconds = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds()
  let formatLabel = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds
  switch (format) {
    case 'YYYY-MM-DD':
      formatLabel = year + '-' + month + '-' + day
      break
    case 'HH:mm:ss':
      formatLabel = hours + ':' + minutes + ':' + seconds
      break
    default:
      break
  }
  return formatLabel
}
export default class Roaming {
  /**
   * 漫游线路
   * @param {*} viewer 必须传入
   * @param {*} options.lines  漫游数据点集合 [{ longitude: , latitude:, time: '2022-6-29 08:00:00' }]
   * @param {*} options.isPathShow 是否显示路径
   * @param {*} options.modelUri 模型地址
   */
  constructor(viewer, options) {
    this.viewer = viewer
    this.entity = null
    this.clock = null
    this.start = null
    this.stop = null
    this.lines = options.lines || []
    this.isPathShow = options.isPathShow || true
    this.modelUri = options.modelUri
    this.property = this.computeRoamingLineProperty()
    this.initRoaming()
  }

  /**
   * 点集合
   */
  computeRoamingLineProperty() {
    Cesium.Math.setRandomNumberSeed(3)

    const start = Cesium.JulianDate.fromDate(new Date(this.lines[0].timestamp))
    const stop = Cesium.JulianDate.fromDate(new Date(this.lines[this.lines.length - 1].timestamp))
    this.start = start
    this.stop = stop

    this.viewer.clock.startTime = start.clone()
    this.viewer.clock.stopTime = stop.clone()
    this.viewer.clock.currentTime = start.clone()
    this.viewer.clock.clockRange = Cesium.ClockRange.CLAMPED // 到达最终时间后停止不循环
    this.viewer.clock.multiplier = 5

    this.viewer.animation.viewModel.dateFormatter = function (date, viewModel) {
      const localDate = Cesium.JulianDate.toDate(date)
      return formatDate(localDate, 'YYYY-MM-DD')
    }
    this.viewer.animation.viewModel.timeFormatter = function (date, viewModel) {
      const localDate = Cesium.JulianDate.toDate(date)
      return formatDate(localDate, 'HH:mm:ss')
    }
    this.viewer.timeline.zoomTo(start, stop)

    const property = new Cesium.SampledPositionProperty()
    let i = 2
    this.lines.forEach((item) => {
      const time = Cesium.JulianDate.fromDate(new Date(item.timestamp))
      const position = Cesium.Cartesian3.fromDegrees(item.longitude, item.latitude, item.height || 2)
      property.addSample(time, position)
    })
    return property
  }

  initRoaming() {
    let img = personImg
    if (this.modelUri == 'car') {
      img = carImg
    }
    this.entity = this.viewer.entities.add({
      availability: new Cesium.TimeIntervalCollection([
        new Cesium.TimeInterval({
          start: this.start,
          stop: this.stop
        })
      ]),

      position: this.property,

      orientation: new Cesium.VelocityOrientationProperty(this.property),

      billboard: {
        image: img,
        width: 30,
        height: 47,
        disableDepthTestDistance: 2000,
        verticalOrigin: Cesium.VerticalOrigin.BOTTOM
      },
      // model: {
      //   uri: this.modelUri,
      //   minimumPixelSize: 40,
      //   maximumSize: 50,
      //   maximumScale: 100
      // },
      viewFrom: new Cesium.Cartesian3(this.lines[0].longitude, this.lines[0].latitude, 100),

      path: {
        resolution: 1,
        material: new Cesium.PolylineGlowMaterialProperty({
          glowPower: 0.1,
          color: Cesium.Color.RED
        }),
        width: 10,
        show: this.isPathShow
      }
    })
    // 点插值
    // this.entity.position.setInterpolationOptions({
    //   interpolationDegree: 5,
    //   interpolationAlgorithm: Cesium.LagrangePolynomialApproximation
    // })

    console.log(this.entity)

    this.viewer.zoomTo(this.entity,new Cesium.HeadingPitchRange (  2.35 , -1 , 200 ))
  }

  /**
   * 获取当前运动位置
   */
  getCurrentPosition() {
    const cartesian3 = position.getValue(this.viewer.clock.currentTime)
    const num = 180 / Math.PI
    const cartographic = Cesium.Cartographic.fromCartesian(cartesian3)
    const longgitude = cartographic.longitude * num
    const latitude = cartographic.latitude * num
    return {
      longgitude,
      latitude
    }
  }

  /**
   * 漫游开始
   */
  startRoaming() {
    this.viewer.clock.shouldAnimate = true
  }

  /**
   * 漫游暂停
   */
  stopRoaming() {
    this.viewer.clock.shouldAnimate = false
  }

  /**
   * 取消漫游
   */
  clearRoaming() {
    this.entity && this.viewer.entities.remove(this.entity)
  }
}
