/**
 * 带高度的多边形样式
 */
// import * as Cesium from 'cesium'

export default class PolygonStyle {
  /**
   * @param options
   * color 颜色 new Cesium.Color(190, 145, 11)
   * outline 是否显示边框  默认 有
   * outlineColor 边框颜色 默认 黑色
   */
  constructor(options) {
    const { color, outline, outlineColor } = options || {}
    this.color = color || Cesium.Color.GREEN.withAlpha(0.5)
    this.outline = outline || false
    this.outlineColor = outlineColor || Cesium.Color.BLACK
  }
}
