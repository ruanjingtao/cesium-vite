// import * as Cesium from 'cesium'

/**
 * 图标的样式类
 */
export default class TextStyle {
  /**
   *
   * @param options
   *  text  显示文字
   *  font 文字大小和字体 默认（'14pt monospace'）
   *  fillColor 填充颜色 默认 new Cesium.Color(190, 145, 11)
   *  outLineColor 边缘颜色 默认 new Cesium.Color(190, 145, 11)
   *  outlineWidth 边缘宽度
   *  offsetX  x偏移
   *  offsetY  y偏移
   */
  constructor(options) {
    const { text, font, fillColor, outlineColor, outlineWidth, offsetX, offsetY, backgroundColor,horizontalOrigin } = options || {}
    this.text = text || ''
    this.font = font || '15px Microsoft Yahei'
    this.fillColor = fillColor || Cesium.Color.fromBytes(255, 255, 255)
    this.outlineColor = outlineColor || Cesium.Color.WHITE
    this.outlineWidth = outlineWidth || 1
    this.pixelOffset = new Cesium.Cartesian2(offsetX || 0, offsetY || -40)
    this.horizontalOrigin = horizontalOrigin || Cesium.HorizontalOrigin.CENTER
    this.backgroundColor = backgroundColor
    this.showBackground = !!backgroundColor
  }
}
