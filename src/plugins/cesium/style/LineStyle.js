// import * as Cesium from 'cesium'

/**
 * 线的样式
 */
export default class LineStyle {
  /**
   * @param options
   *    color 颜色 默认 green
   *    width 宽度 默认 5
   *    dash 是否使用虚线 默认false
   *    dashColor 虚线颜色 默认 green dash设置为false时无效
   *    dashGapColor 虚线间隙的颜色 默认 white dash设置为false时无效
   *    dashLength 虚线长度 默认2 dash设置为false时无效
   */
  constructor(options) {
    const { color, width, dash, dashColor, dashGapColor, dashLength } = options || {}
    this.color = color || Cesium.Color.YELLOW
    this.width = width || 5
    this.dash = dash || false
    this.dashColor = dashColor || Cesium.Color.GREEN
    this.dashGapColor = dashGapColor || Cesium.Color.WHITE
    this.dashLength = dashLength || 2
  }
}
