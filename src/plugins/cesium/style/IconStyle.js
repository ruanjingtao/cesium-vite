// import * as Cesium from 'cesium'

/**
 * 图标的样式类
 */
export default class IconStyle {
  constructor(options) {
    const { url, height, width, offsetX, offsetY, color } = options || {}
    this.url = url || null
    this.height = height || 30
    this.width = width || 30
    this.color = color || Cesium.Color.fromCssColorString('#FFFFFF').withAlpha(0.9)
    this.pixelOffset = new Cesium.Cartesian2(offsetX || 0, offsetY || 0)
  }
}
