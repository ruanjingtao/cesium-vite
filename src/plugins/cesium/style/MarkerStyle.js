/**
 * 标记的样式
 */
import IconStyle from './IconStyle'
import TextStyle from './TextStyle'

export default class MarkerStyle {
  /**
   * @param options
   *    iconStyle icon
   *    textStyle
   */
  constructor(options) {
    const { iconStyle, textStyle } = options || {}
    this.iconStyle = iconStyle || new IconStyle()
    this.textStyle = textStyle || new TextStyle()
  }
}
