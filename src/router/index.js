import {createRouter, createWebHashHistory} from 'vue-router'

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/',
            redirect: '/a'
        },
        {
            path: '/iframe',
            component: () => import('../views/iframe/index.vue')
        },
        {
            path: '/cesium',
            component: () => import('../components/HelloWorld.vue')
        },
        {
            path: '/slot',
            component: () => import('../views/slot/Father.vue')
        },
        {
            path: '/test',
            component: () => import('../components/testDemo.vue')
        },
        {
            path: '/a',
            component: () => import('../components/Test.vue')
        },
        {
            path: '/b',
            component: () => import('../components/TestB.vue')
        },
        {
            path: '/ts',
            component: () => import('../components/TsTest.vue')
        },
        {
            path: '/two',
            component: () => import('../components/TwoCamera.vue')
        }
    ]
})

export default router
